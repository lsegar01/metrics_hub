# Metrics Hub API

FastAPI + postgres based metrics hub API

## env vars

- PG_DATA: local path of the volume used for postges data

```
export PG_DATA=`mktemp -d`
```

- PG_PASS: password for postgres service

```
export PG_PASS=`openssl rand -hex 20`
```

## TEST

> Some of the tests expect $PG_DATA to be empty

- Start db service

```
docker-compose up --build --remove-orphans -d postgresdb
```

- Run tests

```
docker-compose run  --entrypoint "pytest -v" api
```

## Run

- Start services

```
docker-compose up --build --remove-orphans
```


- Use API

```
curl -i -d '{"server_name": "t-094553234", "start_time": "2021-05-17T10:12:33Z", "end_time": "2021-05-17T11:15:19Z"}' -H "Content-Type: application/json" -X POST http://0.0.0.0:8000/process_report
```

```
curl -i http://0.0.0.0:8000/process_outliers
```

```
curl -i http://0.0.0.0:8000/process_statistics
```

