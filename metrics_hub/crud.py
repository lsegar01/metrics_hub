import re
from sqlalchemy.orm import Session
from sqlalchemy import func, select

from . import models, schemas

from .time_utils import get_str_timestamp, get_duration
from .models import EXECUTIONS_TABLE, SERVER_FIELD, START_FIELD, DURATION_FIELD

EXP_DURATION = "SELECT {} FROM {}".format(DURATION_FIELD, EXECUTIONS_TABLE)
EXP_INSERT_EXECUTIONS = "INSERT INTO {}".format(EXECUTIONS_TABLE)

def _get_insert_query(value_server, value_start, value_duration):
    fields_insert_exec = "({}, {}, {})".format(SERVER_FIELD, START_FIELD, DURATION_FIELD)
    return " ".join([
                      EXP_INSERT_EXECUTIONS,
                      fields_insert_exec,
                      "VALUES ('{}', '{}', {});".format(value_server, value_start, value_duration)
                    ])

def _validate_insert_query(insert_query):
    valid_re = "".join(["^",
                        EXP_INSERT_EXECUTIONS,
                        " ",
                        r"\({}, {}, {}\)".format(SERVER_FIELD, START_FIELD, DURATION_FIELD),
                        " ",
                        "VALUES",
                        " ",
                        r"\('[a-zA-z0-9|\.|_|\-]+', '[0-9]{10}', [0-9]{1,10}\)",
                        ";",
                        "$"])
    return re.match(valid_re, insert_query)

def get_duration_mean(db: Session):
    exp_mean = "SELECT avg({}) FROM ({}) as dur_avg;".format(DURATION_FIELD, EXP_DURATION)
    c_mean_scalar = db.execute(exp_mean).scalar()
    return int(round(c_mean_scalar.to_integral_exact()))

def get_duration_stddev(db: Session):
    exp_stddev = "SELECT stddev({}) FROM ({}) as dur_stddev;".format(DURATION_FIELD, EXP_DURATION)
    c_stddev_scalar = db.execute(exp_stddev).scalar()
    return int(round(c_stddev_scalar.to_integral_exact()))

def get_outliers(db: Session):
    d_mean = get_duration_mean(db)
    d_stddev = get_duration_stddev(db)
    low_t = d_mean + 3 * d_stddev
    high_t = d_mean - 3 * d_stddev
    exp_outliers = "SELECT DISTINCT ({0}.{2}) FROM (SELECT {1}.{2} FROM {1} WHERE {1}.{3}<{4} or {1}.{3}>{5}) as {0};".format('t',
                                                                                                                              EXECUTIONS_TABLE,
                                                                                                                              SERVER_FIELD,
                                                                                                                              DURATION_FIELD,
                                                                                                                              low_t,
                                                                                                                              high_t)
    c_outliers = db.execute(exp_outliers).fetchall()
    return [x[0] for x in c_outliers]

def get_reports_count(db: Session):
    exp_count = "SELECT COUNT(*) from {};".format(EXECUTIONS_TABLE)
    c_count = db.execute(exp_count).scalar()
    return c_count

def insert_report(db: Session, report: schemas.ReportIn):
    insert_query = _get_insert_query(report.server_name,
                                     get_str_timestamp(report.start_time),
                                     get_duration(report.start_time, report.end_time))

    if not _validate_insert_query(insert_query):
        raise RuntimeError("Insertion not valid")

    db.execute(insert_query)
    db.commit()

