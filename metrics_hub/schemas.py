from pydantic import BaseModel

class ReportIn(BaseModel):
    server_name: str
    start_time: str
    end_time: str

    class Config:
        orm_mode = True

class StatisticsOut(BaseModel):
    mean: int
    stddev: int

    class Config:
        orm_mode = True


