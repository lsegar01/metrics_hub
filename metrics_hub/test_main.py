from fastapi.testclient import TestClient
import datetime

from .time_utils import DATES_FORMAT
from .main import app
client = TestClient(app)

def test_process_statistics_no_reports():
    """ 
    Check expected error response status and error body field
    Expects 'executions' db to be empty
    """
    response = client.get("/process_statistics") 
    assert response.status_code == 500
    assert response.json().get('error') == "Not enough reports received"


def test_process_outliers_no_reports():
    """ 
    Check expected error response status
    Expects 'executions' db to be empty
    """
    response = client.get("/process_outliers")
    assert response.status_code == 500


def test_report_injection():
    """
    Test the insertion of unvalid report
    """

    _f_rand = datetime.datetime.now().microsecond
    _f_start = datetime.datetime.now().strftime(DATES_FORMAT)
    _f_end = (datetime.datetime.now() + datetime.timedelta(seconds=_f_rand)).strftime(DATES_FORMAT)
    response = client.post("/process_report",
                            json={"server_name": "malicious0', '{}', '{}'); DROP TABLE executions; -- ".format(_f_start, _f_end),
                                   "start_time": _f_start,
                                   "end_time": _f_end})
    assert response.status_code == 400

def test_stastics():
    """ 
    Test Insert reports with fixed duration to check stastics
    Inserts data needed for further tests to suceed
    """
    _expected_mean, _expected_stddev = 43, 22
    _fake_servers = ['talco', 'the_clash', 'evaristo', 'eukz', 'los_de_marras', 'cock_sparrer', 'mcd', 'accidente', 'duelo', 'kaotiko'] 
    _fixed_durations = [13, 22, 26, 38, 36, 42,49, 50, 77, 81]

    for _fake_exec in zip(_fixed_durations, _fake_servers):

        _f_start = datetime.datetime.now().strftime(DATES_FORMAT)
        _f_end = (datetime.datetime.now() + datetime.timedelta(seconds=_fake_exec[0])).strftime(DATES_FORMAT)

        response = client.post("/process_report",
                               json={"server_name": _fake_exec[1],
                                     "start_time": _f_start,
                                     "end_time": _f_end})
        assert response.status_code == 200
        assert response.json() == {}

    response = client.get("/process_statistics") 
    assert response.status_code == 200
    assert response.json().get('mean') == _expected_mean
    assert response.json().get('stddev') == _expected_stddev

def test_process_report():
    """ 
    Test Insert reports with random duration
    Inserts data needed for further tests to suceed
    """
    _fake_servers = ['mcd', 'accidente', 'duelo', 'kaotiko'] 


    for _s in _fake_servers:
        _f_rand = datetime.datetime.now().microsecond
        _f_start = datetime.datetime.now().strftime(DATES_FORMAT)
        _f_end = (datetime.datetime.now() + datetime.timedelta(seconds=_f_rand)).strftime(DATES_FORMAT)

        response = client.post("/process_report",
                               json={"server_name": _s,
                                     "start_time": _f_start,
                                     "end_time": _f_end})
        assert response.status_code == 200
        assert response.json() == {}


def test_process_outliers():
    """ 
    Check expected response status and type of response data
    Expects 'executions' db to be filled by previous tests
    """
    response = client.get("/process_outliers")
    assert response.status_code == 200
    assert type(response.json()) == list


def test_process_statistics():
    """ 
    Check expected response status and type of response data
    Expects 'executions' db to be filled by previous tests
    """
    response = client.get("/process_outliers")
    response = client.get("/process_statistics") 
    assert response.status_code == 200
    assert set(response.json().keys()) == {'mean', 'stddev'}


