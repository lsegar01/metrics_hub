fastapi
psycopg2
pytest
requests
SQLAlchemy
uvicorn[standard]
