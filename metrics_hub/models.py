from sqlalchemy import Table, Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from .database import Base

EXECUTIONS_TABLE = 'executions'
SERVER_FIELD = 'server'
START_FIELD = 'start'
DURATION_FIELD = 'duration'

class ProcessExec(Base):
    __table__ = Table(
        EXECUTIONS_TABLE,
        Base.metadata,
        Column("id", Integer, primary_key=True),
        Column(SERVER_FIELD, String),#, primary_key=True),
        Column(START_FIELD, String),#, primary_key=True),
        Column(DURATION_FIELD, Integer),#, primary_key=True),
    )
