from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os


#SQLALCHEMY_DATABASE_URL = "postgresql://postgres:xxxXX@localhost/postgres"
pg_pass = os.getenv("POSTGRES_PASSWORD", "yyyyyy")
pg_host = os.getenv("PGHOST", "localhost")
SQLALCHEMY_DATABASE_URL = "postgresql://postgres:{}@{}:5432/postgres".format(pg_pass, pg_host)
#SQLALCHEMY_DATABASE_URL = "postgresql://postgres:xxxXX@postgres/postgres"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL
#    connect_args={"check_same_thread": False} # is needed only for SQLite
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()

