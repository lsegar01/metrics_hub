import datetime

DATES_FORMAT = "%Y-%m-%dT%H:%M:%SZ"


def get_str_timestamp(lf_date: str) -> str:
    """Return a shorter representation of a date str

    @param lf_date: long formated str date
    """
    return datetime.datetime.strptime(lf_date, DATES_FORMAT).strftime('%s')


def get_duration(start_time:str, end_time:str) -> int:
    """Returns the duration in seconds of a process

    @param start_time: time in ISO 8601 when the process started
    @param end_time: time in ISO 8601 when the process finished
    """
    _d_s = datetime.datetime.strptime(start_time, DATES_FORMAT)
    _d_f = datetime.datetime.strptime(end_time, DATES_FORMAT)
    return int((_d_f - _d_s).total_seconds())

