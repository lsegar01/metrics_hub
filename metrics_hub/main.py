from sqlalchemy.orm import Session
from fastapi import FastAPI, Depends
from fastapi.responses import JSONResponse
from .database import SessionLocal, engine
from . import models, schemas, crud

models.Base.metadata.create_all(bind=engine)
app = FastAPI()

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.get("/process_statistics", response_model=schemas.StatisticsOut, status_code=200)
async def process_statistics(db: Session = Depends(get_db)):
    _c_count = crud.get_reports_count(db=db)
    if _c_count < 10:
       return JSONResponse(status_code=500,
                           content={"error": "Not enough reports received"})
    else:
        _c_mean = crud.get_duration_mean(db=db)
        _c_stddev = crud.get_duration_stddev(db=db)
        return schemas.StatisticsOut(mean=_c_mean, stddev=_c_stddev)

@app.get('/process_outliers', status_code=200)
async def process_outliers(db: Session = Depends(get_db)):
    try:
        _c = crud.get_outliers(db=db)
        return _c
    except AttributeError as e:
        return JSONResponse(status_code=500,
                            content={"error": "Could not process stastics. Is de db populated?"})

@app.post('/process_report', status_code=200)
async def process_report(report: schemas.ReportIn, db: Session = Depends(get_db)):
    try:
        _c = crud.insert_report(db=db, report=report)
        return {}
    except (ValueError, RuntimeError) as e:
        return JSONResponse(status_code=400,
                            content={"error": "Incoming request data did not pass validation"})

